Version 4
SHEET 1 1232 744
WIRE 256 -208 -176 -208
WIRE 256 -64 256 -208
WIRE 192 -16 32 -16
WIRE 336 32 256 32
WIRE 336 96 336 32
WIRE 480 96 336 96
WIRE 544 96 480 96
WIRE 32 112 32 -16
WIRE 336 128 336 96
WIRE 480 128 480 96
WIRE 480 224 480 208
WIRE 544 224 672 144
WIRE 544 224 480 224
WIRE 336 256 336 208
WIRE 592 352 256 352
WIRE -176 384 -176 -208
WIRE -112 384 -176 384
WIRE 64 384 -112 384
WIRE 256 384 256 352
WIRE 256 384 64 384
WIRE 512 384 384 384
WIRE 544 384 544 224
WIRE 624 384 544 384
WIRE 512 416 512 384
WIRE 544 416 544 384
WIRE 624 432 624 384
WIRE 256 448 256 384
WIRE 592 448 592 352
WIRE 592 448 560 448
WIRE 64 496 64 464
WIRE 192 496 64 496
WIRE 64 512 64 496
WIRE -112 528 -112 384
WIRE 256 576 256 544
WIRE 384 576 384 384
WIRE 384 576 256 576
WIRE 528 576 528 480
WIRE 624 576 624 512
WIRE 624 576 528 576
WIRE 800 576 704 576
WIRE 64 608 64 592
WIRE 256 608 256 576
WIRE 704 608 704 576
WIRE 624 624 624 576
WIRE 656 624 624 624
WIRE 384 672 384 576
WIRE 656 672 384 672
FLAG 32 192 0
FLAG 336 256 0
FLAG -96 -528 0
FLAG -176 -432 0
FLAG -112 608 0
FLAG 64 608 0
FLAG 256 688 0
FLAG 496 448 0
FLAG 704 688 0
FLAG 816 656 0
FLAG 800 656 0
SYMBOL voltage 32 96 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
WINDOW 3 -137 128 Left 2
SYMATTR InstName V1
SYMATTR Value SINE(1.65 0.15 {FM} 0 0 0)
SYMBOL cap 656 80 R0
SYMATTR InstName C1
SYMATTR Value {CDUT}
SYMBOL npn 192 -64 R0
SYMATTR InstName Q1
SYMATTR Value BCW60D
SYMBOL voltage -96 -624 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V3
SYMATTR Value PULSE(2 3 100u 100n 100n 100u 200u)
SYMBOL voltage -176 -528 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 0
WINDOW 3 24 44 Left 2
SYMATTR Value2 AC 1
SYMATTR Value 2.5
SYMATTR InstName V4
SYMBOL voltage -112 512 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V5
SYMATTR Value 5
SYMBOL res 48 368 R0
SYMATTR InstName R2
SYMATTR Value 100
SYMBOL res 48 496 R0
SYMATTR InstName R3
SYMATTR Value 100
SYMBOL npn 192 448 R0
SYMATTR InstName Q2
SYMATTR Value BCW60D
SYMBOL res 240 592 R0
SYMATTR InstName R4
SYMATTR Value 100
SYMBOL Opamps\\LT1492 592 448 R90
SYMATTR InstName U1
SYMBOL res 608 416 R0
SYMATTR InstName R5
SYMATTR Value 300
SYMBOL res 464 112 R0
SYMATTR InstName R6
SYMATTR Value 50
SYMBOL g2 704 704 M180
WINDOW 3 -32 -21 Left 2
SYMATTR InstName G1
SYMATTR Value {-1/100}
SYMBOL voltage 800 560 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value 3.3
SYMBOL res 320 112 R0
SYMATTR InstName R1
SYMATTR Value 10
TEXT 0 280 Left 2 !.tran {2/FM}
TEXT 816 0 Left 2 !;.step param CDUT list 10n 100n 1u
TEXT -64 -448 Left 2 !;.ac dec 100 1 100K
TEXT 832 -48 Left 2 !.param CDUT=1m FM=10K
TEXT 592 288 Left 2 !.step param FM list 10 10000
TEXT 728 440 Left 2 !.step temp list 0 25 50
